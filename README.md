### Bazel Smithy Test

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tvolk131/bazel_smithy_test)

This repo demonstrates an issue with the `smd` macro in smithy.
To reproduce the error, open the project in Gitpod and run `bazel build //smithy_client:app`.
Exact dependencies and code (with slight tweaks to work with bazel) were copied from [here](https://github.com/rbalicki2/rustactoe/tree/78e20cb04be08e6b7e56a72f3f6fe0f14a18dda4).