# 1. Ensure cargo raze is installed.
cargo install cargo-raze

# 2. Remove all previously generated files.
rm remote -r
rm BUILD.bazel
rm crates.bzl

# 3. Re-generate lockfile.
# This automatically updates some dependencies, and makes sure
# that any manual changes made in the toml file will be reflected.
cargo generate-lockfile

# 4. This re-generates the deleted files from step 2.
cargo raze