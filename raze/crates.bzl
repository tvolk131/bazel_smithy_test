"""
@generated
cargo-raze generated Bazel file.

DO NOT EDIT! Replaced on runs of cargo-raze
"""

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")  # buildifier: disable=load

def cards_mono_fetch_remote_crates():
    """This function defines a collection of repos and should be called in a WORKSPACE file"""
    maybe(
        http_archive,
        name = "cards_mono__arrayvec__0_5_2",
        url = "https://crates.io/api/v1/crates/arrayvec/0.5.2/download",
        type = "tar.gz",
        sha256 = "23b62fc65de8e4e7f52534fb52b0f3ed04746ae267519eef2a83941e8085068b",
        strip_prefix = "arrayvec-0.5.2",
        build_file = Label("//raze/remote:BUILD.arrayvec-0.5.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bitflags__1_2_1",
        url = "https://crates.io/api/v1/crates/bitflags/1.2.1/download",
        type = "tar.gz",
        sha256 = "cf1de2fe8c75bc145a2f577add951f8134889b4795d47466a54a5c846d691693",
        strip_prefix = "bitflags-1.2.1",
        build_file = Label("//raze/remote:BUILD.bitflags-1.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bumpalo__3_6_1",
        url = "https://crates.io/api/v1/crates/bumpalo/3.6.1/download",
        type = "tar.gz",
        sha256 = "63396b8a4b9de3f4fdfb320ab6080762242f66a8ef174c49d8e19b674db4cdbe",
        strip_prefix = "bumpalo-3.6.1",
        build_file = Label("//raze/remote:BUILD.bumpalo-3.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cfg_if__0_1_10",
        url = "https://crates.io/api/v1/crates/cfg-if/0.1.10/download",
        type = "tar.gz",
        sha256 = "4785bdd1c96b2a846b2bd7cc02e86b6b3dbf14e7e53446c4f54c92a361040822",
        strip_prefix = "cfg-if-0.1.10",
        build_file = Label("//raze/remote:BUILD.cfg-if-0.1.10.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cfg_if__1_0_0",
        url = "https://crates.io/api/v1/crates/cfg-if/1.0.0/download",
        type = "tar.gz",
        sha256 = "baf1de4339761588bc0619e3cbc0120ee582ebb74b53b4efbf79117bd2da40fd",
        strip_prefix = "cfg-if-1.0.0",
        build_file = Label("//raze/remote:BUILD.cfg-if-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__console_error_panic_hook__0_1_6",
        url = "https://crates.io/api/v1/crates/console_error_panic_hook/0.1.6/download",
        type = "tar.gz",
        sha256 = "b8d976903543e0c48546a91908f21588a680a8c8f984df9a5d69feccb2b2a211",
        strip_prefix = "console_error_panic_hook-0.1.6",
        build_file = Label("//raze/remote:BUILD.console_error_panic_hook-0.1.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__custom_derive__0_1_7",
        url = "https://crates.io/api/v1/crates/custom_derive/0.1.7/download",
        type = "tar.gz",
        sha256 = "ef8ae57c4978a2acd8b869ce6b9ca1dfe817bff704c220209fdef2c0b75a01b9",
        strip_prefix = "custom_derive-0.1.7",
        build_file = Label("//raze/remote:BUILD.custom_derive-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__either__1_6_1",
        url = "https://crates.io/api/v1/crates/either/1.6.1/download",
        type = "tar.gz",
        sha256 = "e78d4f1cc4ae33bbfc157ed5d5a5ef3bc29227303d595861deb238fcec4e9457",
        strip_prefix = "either-1.6.1",
        build_file = Label("//raze/remote:BUILD.either-1.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__enum_derive__0_1_7",
        url = "https://crates.io/api/v1/crates/enum_derive/0.1.7/download",
        type = "tar.gz",
        sha256 = "406ac2a8c9eedf8af9ee1489bee9e50029278a6456c740f7454cf8a158abc816",
        strip_prefix = "enum_derive-0.1.7",
        build_file = Label("//raze/remote:BUILD.enum_derive-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures__0_1_31",
        url = "https://crates.io/api/v1/crates/futures/0.1.31/download",
        type = "tar.gz",
        sha256 = "3a471a38ef8ed83cd6e40aa59c1ffe17db6855c18e3604d9c4ed8c08ebc28678",
        strip_prefix = "futures-0.1.31",
        build_file = Label("//raze/remote:BUILD.futures-0.1.31.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__itertools__0_8_2",
        url = "https://crates.io/api/v1/crates/itertools/0.8.2/download",
        type = "tar.gz",
        sha256 = "f56a2d0bc861f9165be4eb3442afd3c236d8a98afd426f65d92324ae1091a484",
        strip_prefix = "itertools-0.8.2",
        build_file = Label("//raze/remote:BUILD.itertools-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__itoa__0_4_7",
        url = "https://crates.io/api/v1/crates/itoa/0.4.7/download",
        type = "tar.gz",
        sha256 = "dd25036021b0de88a0aff6b850051563c6516d0bf53f8638938edbb9de732736",
        strip_prefix = "itoa-0.4.7",
        build_file = Label("//raze/remote:BUILD.itoa-0.4.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__js_sys__0_3_50",
        url = "https://crates.io/api/v1/crates/js-sys/0.3.50/download",
        type = "tar.gz",
        sha256 = "2d99f9e3e84b8f67f846ef5b4cbbc3b1c29f6c759fcbce6f01aa0e73d932a24c",
        strip_prefix = "js-sys-0.3.50",
        build_file = Label("//raze/remote:BUILD.js-sys-0.3.50.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lazy_static__1_4_0",
        url = "https://crates.io/api/v1/crates/lazy_static/1.4.0/download",
        type = "tar.gz",
        sha256 = "e2abad23fbc42b3700f2f279844dc832adb2b2eb069b2df918f455c4e18cc646",
        strip_prefix = "lazy_static-1.4.0",
        build_file = Label("//raze/remote:BUILD.lazy_static-1.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lexical_core__0_7_5",
        url = "https://crates.io/api/v1/crates/lexical-core/0.7.5/download",
        type = "tar.gz",
        sha256 = "21f866863575d0e1d654fbeeabdc927292fdf862873dc3c96c6f753357e13374",
        strip_prefix = "lexical-core-0.7.5",
        build_file = Label("//raze/remote:BUILD.lexical-core-0.7.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__log__0_4_14",
        url = "https://crates.io/api/v1/crates/log/0.4.14/download",
        type = "tar.gz",
        sha256 = "51b9bbe6c47d51fc3e1a9b945965946b4c44142ab8792c50835a980d362c2710",
        strip_prefix = "log-0.4.14",
        build_file = Label("//raze/remote:BUILD.log-0.4.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__memchr__2_3_4",
        url = "https://crates.io/api/v1/crates/memchr/2.3.4/download",
        type = "tar.gz",
        sha256 = "0ee1c47aaa256ecabcaea351eae4a9b01ef39ed810004e298d2511ed284b1525",
        strip_prefix = "memchr-2.3.4",
        build_file = Label("//raze/remote:BUILD.memchr-2.3.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__nom__4_2_3",
        url = "https://crates.io/api/v1/crates/nom/4.2.3/download",
        type = "tar.gz",
        sha256 = "2ad2a91a8e869eeb30b9cb3119ae87773a8f4ae617f41b1eb9c154b2905f7bd6",
        strip_prefix = "nom-4.2.3",
        build_file = Label("//raze/remote:BUILD.nom-4.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__nom__5_1_2",
        url = "https://crates.io/api/v1/crates/nom/5.1.2/download",
        type = "tar.gz",
        sha256 = "ffb4262d26ed83a1c0a33a38fe2bb15797329c85770da05e6b828ddb782627af",
        strip_prefix = "nom-5.1.2",
        build_file = Label("//raze/remote:BUILD.nom-5.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro2__0_4_30",
        url = "https://crates.io/api/v1/crates/proc-macro2/0.4.30/download",
        type = "tar.gz",
        sha256 = "cf3d2011ab5c909338f7887f4fc896d35932e29146c12c8d01da6b22a80ba759",
        strip_prefix = "proc-macro2-0.4.30",
        build_file = Label("//raze/remote:BUILD.proc-macro2-0.4.30.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro2__1_0_26",
        url = "https://crates.io/api/v1/crates/proc-macro2/1.0.26/download",
        type = "tar.gz",
        sha256 = "a152013215dca273577e18d2bf00fa862b89b24169fb78c4c95aeb07992c9cec",
        strip_prefix = "proc-macro2-1.0.26",
        build_file = Label("//raze/remote:BUILD.proc-macro2-1.0.26.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__quote__0_6_13",
        url = "https://crates.io/api/v1/crates/quote/0.6.13/download",
        type = "tar.gz",
        sha256 = "6ce23b6b870e8f94f81fb0a363d65d86675884b34a09043c81e5562f11c1f8e1",
        strip_prefix = "quote-0.6.13",
        build_file = Label("//raze/remote:BUILD.quote-0.6.13.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__quote__1_0_9",
        url = "https://crates.io/api/v1/crates/quote/1.0.9/download",
        type = "tar.gz",
        sha256 = "c3d0b9745dc2debf507c8422de05d7226cc1f0644216dfdfead988f9b1ab32a7",
        strip_prefix = "quote-1.0.9",
        build_file = Label("//raze/remote:BUILD.quote-1.0.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ryu__1_0_5",
        url = "https://crates.io/api/v1/crates/ryu/1.0.5/download",
        type = "tar.gz",
        sha256 = "71d301d4193d031abdd79ff7e3dd721168a9572ef3fe51a1517aba235bd8f86e",
        strip_prefix = "ryu-1.0.5",
        build_file = Label("//raze/remote:BUILD.ryu-1.0.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__scoped_tls__1_0_0",
        url = "https://crates.io/api/v1/crates/scoped-tls/1.0.0/download",
        type = "tar.gz",
        sha256 = "ea6a9290e3c9cf0f18145ef7ffa62d68ee0bf5fcd651017e586dc7fd5da448c2",
        strip_prefix = "scoped-tls-1.0.0",
        build_file = Label("//raze/remote:BUILD.scoped-tls-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__scoped_css__0_0_1",
        url = "https://crates.io/api/v1/crates/scoped_css/0.0.1/download",
        type = "tar.gz",
        sha256 = "e86bf0ff32517a749b0865975e6bd166eedd2587b4f6543152e2aa087f6361b1",
        strip_prefix = "scoped_css-0.0.1",
        build_file = Label("//raze/remote:BUILD.scoped_css-0.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde__1_0_125",
        url = "https://crates.io/api/v1/crates/serde/1.0.125/download",
        type = "tar.gz",
        sha256 = "558dc50e1a5a5fa7112ca2ce4effcb321b0300c0d4ccf0776a9f60cd89031171",
        strip_prefix = "serde-1.0.125",
        build_file = Label("//raze/remote:BUILD.serde-1.0.125.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_derive__1_0_125",
        url = "https://crates.io/api/v1/crates/serde_derive/1.0.125/download",
        type = "tar.gz",
        sha256 = "b093b7a2bb58203b5da3056c05b4ec1fed827dcfdb37347a8841695263b3d06d",
        strip_prefix = "serde_derive-1.0.125",
        build_file = Label("//raze/remote:BUILD.serde_derive-1.0.125.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_json__1_0_64",
        url = "https://crates.io/api/v1/crates/serde_json/1.0.64/download",
        type = "tar.gz",
        sha256 = "799e97dc9fdae36a5c8b8f2cae9ce2ee9fdce2058c57a93e6099d919fd982f79",
        strip_prefix = "serde_json-1.0.64",
        build_file = Label("//raze/remote:BUILD.serde_json-1.0.64.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smd_macro__0_0_7",
        url = "https://crates.io/api/v1/crates/smd_macro/0.0.7/download",
        type = "tar.gz",
        sha256 = "5af3a85611bc0d4138a8bfdd7700dd5a16a1aed781f4f7326d2e873ee7048602",
        strip_prefix = "smd_macro-0.0.7",
        build_file = Label("//raze/remote:BUILD.smd_macro-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy/0.0.7/download",
        type = "tar.gz",
        sha256 = "00b3c43789dc877cfe5707411d4aefb56ab986b53d8a520496f0fda88de08aa9",
        strip_prefix = "smithy-0.0.7",
        build_file = Label("//raze/remote:BUILD.smithy-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy_core__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy_core/0.0.7/download",
        type = "tar.gz",
        sha256 = "4ea5c9f0aa281ce00937cf6d96510afe8c118e595527be14e7812b75608e937b",
        strip_prefix = "smithy_core-0.0.7",
        build_file = Label("//raze/remote:BUILD.smithy_core-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy_types__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy_types/0.0.7/download",
        type = "tar.gz",
        sha256 = "6e99122db47faeea05dc2377ad7b517de1268ecdc9aa82d1318101d06104d783",
        strip_prefix = "smithy_types-0.0.7",
        build_file = Label("//raze/remote:BUILD.smithy_types-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__static_assertions__1_1_0",
        url = "https://crates.io/api/v1/crates/static_assertions/1.1.0/download",
        type = "tar.gz",
        sha256 = "a2eb9349b6444b326872e140eb1cf5e7c522154d69e7a0ffb0fb81c06b37543f",
        strip_prefix = "static_assertions-1.1.0",
        build_file = Label("//raze/remote:BUILD.static_assertions-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__syn__1_0_69",
        url = "https://crates.io/api/v1/crates/syn/1.0.69/download",
        type = "tar.gz",
        sha256 = "48fe99c6bd8b1cc636890bcc071842de909d902c81ac7dab53ba33c421ab8ffb",
        strip_prefix = "syn-1.0.69",
        build_file = Label("//raze/remote:BUILD.syn-1.0.69.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_xid__0_1_0",
        url = "https://crates.io/api/v1/crates/unicode-xid/0.1.0/download",
        type = "tar.gz",
        sha256 = "fc72304796d0818e357ead4e000d19c9c174ab23dc11093ac919054d20a6a7fc",
        strip_prefix = "unicode-xid-0.1.0",
        build_file = Label("//raze/remote:BUILD.unicode-xid-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_xid__0_2_1",
        url = "https://crates.io/api/v1/crates/unicode-xid/0.2.1/download",
        type = "tar.gz",
        sha256 = "f7fe0bb3479651439c9112f72b6c505038574c9fbb575ed1bf3b797fa39dd564",
        strip_prefix = "unicode-xid-0.2.1",
        build_file = Label("//raze/remote:BUILD.unicode-xid-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__version_check__0_1_5",
        url = "https://crates.io/api/v1/crates/version_check/0.1.5/download",
        type = "tar.gz",
        sha256 = "914b1a6776c4c929a602fafd8bc742e06365d4bcbe48c30f9cca5824f70dc9dd",
        strip_prefix = "version_check-0.1.5",
        build_file = Label("//raze/remote:BUILD.version_check-0.1.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__version_check__0_9_3",
        url = "https://crates.io/api/v1/crates/version_check/0.9.3/download",
        type = "tar.gz",
        sha256 = "5fecdca9a5291cc2b8dcf7dc02453fee791a280f3743cb0905f8822ae463b3fe",
        strip_prefix = "version_check-0.9.3",
        build_file = Label("//raze/remote:BUILD.version_check-0.9.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen/0.2.73/download",
        type = "tar.gz",
        sha256 = "83240549659d187488f91f33c0f8547cbfef0b2088bc470c116d1d260ef623d9",
        strip_prefix = "wasm-bindgen-0.2.73",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_backend__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-backend/0.2.73/download",
        type = "tar.gz",
        sha256 = "ae70622411ca953215ca6d06d3ebeb1e915f0f6613e3b495122878d7ebec7dae",
        strip_prefix = "wasm-bindgen-backend-0.2.73",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-backend-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_futures__0_3_27",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-futures/0.3.27/download",
        type = "tar.gz",
        sha256 = "83420b37346c311b9ed822af41ec2e82839bfe99867ec6c54e2da43b7538771c",
        strip_prefix = "wasm-bindgen-futures-0.3.27",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-futures-0.3.27.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_macro__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-macro/0.2.73/download",
        type = "tar.gz",
        sha256 = "3e734d91443f177bfdb41969de821e15c516931c3c3db3d318fa1b68975d0f6f",
        strip_prefix = "wasm-bindgen-macro-0.2.73",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-macro-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_macro_support__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-macro-support/0.2.73/download",
        type = "tar.gz",
        sha256 = "d53739ff08c8a68b0fdbcd54c372b8ab800b1449ab3c9d706503bc7dd1621b2c",
        strip_prefix = "wasm-bindgen-macro-support-0.2.73",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-macro-support-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_shared__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-shared/0.2.73/download",
        type = "tar.gz",
        sha256 = "d9a543ae66aa233d14bb765ed9af4a33e81b8b58d1584cf1b47ff8cd0b9e4489",
        strip_prefix = "wasm-bindgen-shared-0.2.73",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-shared-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_test__0_2_50",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-test/0.2.50/download",
        type = "tar.gz",
        sha256 = "a2d9693b63a742d481c7f80587e057920e568317b2806988c59cd71618bc26c1",
        strip_prefix = "wasm-bindgen-test-0.2.50",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-test-0.2.50.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_test_macro__0_2_50",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-test-macro/0.2.50/download",
        type = "tar.gz",
        sha256 = "0789dac148a8840bbcf9efe13905463b733fa96543bfbf263790535c11af7ba5",
        strip_prefix = "wasm-bindgen-test-macro-0.2.50",
        build_file = Label("//raze/remote:BUILD.wasm-bindgen-test-macro-0.2.50.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__web_sys__0_3_50",
        url = "https://crates.io/api/v1/crates/web-sys/0.3.50/download",
        type = "tar.gz",
        sha256 = "a905d57e488fec8861446d3393670fb50d27a262344013181c2cdf9fff5481be",
        strip_prefix = "web-sys-0.3.50",
        build_file = Label("//raze/remote:BUILD.web-sys-0.3.50.bazel"),
    )
